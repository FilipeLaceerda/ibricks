import 'package:flutter/material.dart';
import 'package:frontend/view/components/avaliation-rating.dart';
import 'package:frontend/view/components/list_machine.dart';
import '../../models/feeds.model.dart';

class ListMachine extends StatefulWidget {
  final Lojas loja;
  const ListMachine({super.key, required this.loja});

  @override
  State<ListMachine> createState() => _ListMachineState();
}

class _ListMachineState extends State<ListMachine> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.indigo),
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(widget.loja.name),
        ),
        body: Column(
            children: [
              AvaliationRating(storesReviewId: widget.loja.id),
              ListMachines(storesId: widget.loja.id)
            ],
        ),
      ),
    );
  }
}
