import 'package:flutter/material.dart';
import 'package:frontend/view/components/form_sign_up.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key, required this.formSignUpContext});
  final BuildContext formSignUpContext;

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cadastro',
      theme: ThemeData(primarySwatch: Colors.indigo),
      home: Scaffold(
        appBar: AppBar(title: const Text('Cadastro')),
        body: FormSignUp(formSignUpContext: widget.formSignUpContext),
      ),
    );
  }
}
