import 'package:flutter/material.dart';
import 'package:frontend/controller/services/avaliation_service.dart';

import '../../models/coments_model.dart';

class CommentsPage extends StatefulWidget {
  final int storeReviewId;

  const CommentsPage({super.key, required this.storeReviewId});

  @override
  State<CommentsPage> createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {
  List<Coments> _coments = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      _loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    final TextEditingController _comentController = TextEditingController();
    final FocusNode _focusNode = FocusNode();

    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.indigo),
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text('Comentários'),
        ),
        body: Column(
          children: [
            Expanded(
              child: ListView.builder(
                itemCount: _coments.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    child: ListTile(
                      title: Text(_coments[index].coment),
                    ),
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 32.0),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Digite seu comentário...',
                      ),
                      controller: _comentController,
                    ),
                  ),
                  IconButton(
                      onPressed: () {
                        _registerComents(_comentController.text, widget.storeReviewId);
                        _comentController.clear();
                      },
                      icon: Icon(Icons.send))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _loadData() async {
    List<Coments> coments = await getListComments(widget.storeReviewId);
    setState(() {
      _coments = coments;
    });
  }

  void _registerComents(String text, int storeReviewId) async {
    await registerComent(text, storeReviewId);
    _loadData();
  }
}
