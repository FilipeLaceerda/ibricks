import 'package:flutter/material.dart';
import 'package:frontend/view/pages/feed.dart';
import 'package:frontend/view/pages/sign_up.dart';
import 'package:frontend/controller/services/login_service.dart';

void main() {
  runApp(const LoginPage());
}

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'IBricks',
      theme: ThemeData(primarySwatch: Colors.indigo),
      home: Form(
        key: _formKey,
        child: Scaffold(
          appBar: AppBar(
            leading: Icon(Icons.account_circle_outlined),
            title: Text('Login'),
          ),
          body: Container(
              height: 500,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset('assets/logo.png'),
                    TextFormField(
                      onChanged: (value) {
                        setState(() {});
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Por favor digita o email.';
                        }
                        return null;
                      },
                      controller: emailController,
                      autofocus: true,
                      keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(
                          labelText: 'Email', hintText: 'Ex: email@email.com'),
                    ),
                    TextFormField(
                      onChanged: (value) {
                        setState(() {});
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Por favor digita a senha.';
                        }
                        return null;
                      },
                      controller: passwordController,
                      keyboardType: TextInputType.visiblePassword,
                      decoration: const InputDecoration(
                        labelText: 'Senha',
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () async {
                             dynamic token = await LoginService().login(
                                emailController.text, passwordController.text);
                               Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const FeedPage()));
                        },
                        child: const Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Entrar',
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              )
                            ])),
                    ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (contextNew) => SignUpPage(formSignUpContext: contextNew,)));
                        },
                        child: const Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Cadastrar',
                              style: TextStyle(fontSize: 16),
                            )
                          ],
                        ))
                  ],
                ),
              )),
        ),
      ),
    );
  }
}
