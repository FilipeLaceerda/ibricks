import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:frontend/controller/services/feeds.dart';
import 'package:frontend/models/feeds.model.dart';

import '../pages/list_machine.dart';

class ListMachineFeed extends StatefulWidget{

  const ListMachineFeed({super.key});

  @override
  State<ListMachineFeed> createState() => _ListMachineState();
}

class _ListMachineState extends State<ListMachineFeed> {
  List<Machines> _machines = [];

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getMachines(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            print(snapshot.error);
            return const Center(
              child: Text('Erro ao carregar os dados.'),
            );
          } else {
            return SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: List.generate(
                  snapshot.data!.length,
                      (index) => InkWell(
                    onTap: () {
                      //Navigator.push(
                        //context
                        //MaterialPageRoute(
                          // builder: (context) => ListMachine(
                          //   loja: snapshot.data[index],
                          // ),
                       // ),
                      //);
                    },
                    child: Card(
                      child: Container(
                        width: 250,
                        height: 300,// Ajuste conforme necessário
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: Colors.black26,
                              ),
                              width: 230,
                              height: 150, // Ajuste conforme necessário
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Image.asset("assets/fachada.jpg", fit: BoxFit.cover),
                              ),
                            ),
                            Container(
                              height: 150,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          snapshot.data![index].nome,
                                          style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                                        ),
                                        Text('Tipo: ${snapshot.data![index].tipo}'),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Text('Preço: ${snapshot.data![index].priceProduct}'),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
        }
    );
  }
}