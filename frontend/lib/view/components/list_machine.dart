import 'package:flutter/material.dart';
import 'package:frontend/controller/services/list_machines.dart';

class ListMachines extends StatefulWidget {
  final int storesId;

  const ListMachines({super.key, required this.storesId});

  @override
  State<ListMachines> createState() => _ListMachinesState();
}

class _ListMachinesState extends State<ListMachines> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getListMachines(widget.storesId),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return const Center(
              child: Text('Erro ao carregar os dados.'),
            );
          } else {
            return Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: GridView.builder(
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10),
                    itemCount: snapshot.data!.length,
                    itemBuilder: (BuildContext context, int index){
                      return Container(
                        color: Color.fromARGB(200, 217, 217, 217),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Container(height: 100, child: Image.asset("assets/tools.jpg", fit: BoxFit.cover,)),
                            Text(snapshot.data![index].nome),
                            Text('R\$ ${snapshot.data![index].price_product.toStringAsFixed(2)} por unidade')
                          ],
                        ),
                      );
                    }
                ),
              ),
            );
          }
        });
  }
}
