import 'package:flutter/material.dart';
import 'package:frontend/controller/services/avaliation_service.dart';
import 'package:frontend/models/coments_model.dart';
import 'package:frontend/models/rating_model.dart';
import 'package:frontend/view/components/count_comments.dart';

class AvaliationRating extends StatefulWidget {
  final int storesReviewId;

  const AvaliationRating({super.key, required int this.storesReviewId});

  @override
  State<AvaliationRating> createState() => _AvaliationRatingState();
}

class _AvaliationRatingState extends State<AvaliationRating> {
  Rating _rating = Rating(rating: 0.0);
  CountComent _countComents = CountComent(count: 0);

  @override
  void initState() {
    super.initState();
    _loadRating();
    _loadCountComents();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Icon(
                Icons.star,
                color: Colors.amber,
              ),
              Text(_rating.rating.toStringAsFixed(2))
            ],
          ),
        ),
        CountComments(countComents: _countComents.count, storeReviewId: widget.storesReviewId,)
      ],
    );
  }

  void _loadRating() async {
    Rating rating = await getRating(widget.storesReviewId);
    setState(() {
      _rating = rating;
    });
  }

  void _loadCountComents() async {
    CountComent countComent = await getCountComent(widget.storesReviewId);
    setState(() {
      _countComents = countComent;
    });
  }
}
