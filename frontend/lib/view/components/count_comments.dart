import 'package:flutter/material.dart';
import 'package:frontend/view/pages/comment.dart';

class CountComments extends StatefulWidget {
  final int countComents;
  final int storeReviewId;

  const CountComments(
      {super.key, required this.countComents, required this.storeReviewId});

  @override
  State<CountComments> createState() => _CountCommentsState();
}

class _CountCommentsState extends State<CountComments> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                     CommentsPage(storeReviewId: widget.storeReviewId)));
      },
      child: Padding(
        padding: EdgeInsets.all(12),
        child: Text('${widget.countComents} comentários'),
      ),
    );
  }
}
