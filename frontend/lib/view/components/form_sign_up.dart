import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:frontend/controller/services/sign_up_service.dart';
import 'package:brasil_fields/brasil_fields.dart';

import '../pages/login.dart';

class FormSignUp extends StatefulWidget {
  const FormSignUp({super.key, required this.formSignUpContext});

  final BuildContext formSignUpContext;

  @override
  State<FormSignUp> createState() => _FormSignUpState();
}

class _FormSignUpState extends State<FormSignUp> {
  String selectedAccountType = 'Pessoa Física';
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    TextEditingController typeAccountController = TextEditingController();
    TextEditingController phoneController = TextEditingController();
    TextEditingController streetAdressController = TextEditingController();
    TextEditingController typeDocument = TextEditingController();
    TextEditingController description = TextEditingController();
    TextEditingController name = TextEditingController();
    return Form(
      key: _formKey,
      child: Center(
        child: SingleChildScrollView(
          child: Container(
            width: 385,
            height: 650,
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButtonFormField<String>(
                    value: selectedAccountType,
                    hint: const Text('Selecione o tipo de conta'),
                    items: ['Pessoa Física', 'Pessoa Jurídica']
                        .map((String accountType) {
                      return DropdownMenuItem<String>(
                        value: accountType,
                        child: Text(accountType),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      setState(() {
                        selectedAccountType = value!;
                        selectedAccountType == 'Pessoa Física'
                            ? typeAccountController.value =
                                'Individual' as TextEditingValue
                            : typeAccountController.value =
                                'Empresarial' as TextEditingValue;
                      });
                    },
                  ),
                ),
                if (selectedAccountType == 'Pessoa Física')
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: typeDocument,
                      keyboardType: TextInputType.text,
                      decoration: const InputDecoration(
                        hintText: 'Documento',
                        fillColor: Colors.white70,
                        filled: true,
                      ),
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        CpfInputFormatter()
                      ],
                    ),
                  ),
                if (selectedAccountType == 'Pessoa Jurídica')
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        TextFormField(
                          controller: typeDocument,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            hintText: 'Documento',
                            fillColor: Colors.white70,
                            filled: true,
                          ),
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                            CnpjInputFormatter()
                          ],
                        ),
                      ],
                    ),
                  ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: description,
                    keyboardType: TextInputType.text,
                    decoration: const InputDecoration(
                      hintText: 'Descrição',
                      fillColor: Colors.white70,
                      filled: true,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: name,
                    keyboardType: TextInputType.text,
                    decoration: const InputDecoration(
                      hintText: 'Nome',
                      fillColor: Colors.white70,
                      filled: true,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      hintText: 'Email',
                      fillColor: Colors.white70,
                      filled: true,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: passwordController,
                    keyboardType: TextInputType.visiblePassword,
                    decoration: const InputDecoration(
                      hintText: 'Senha',
                      fillColor: Colors.white70,
                      filled: true,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: phoneController,
                    keyboardType: TextInputType.phone,
                    decoration: const InputDecoration(
                      hintText: 'Telefone',
                      fillColor: Colors.white70,
                      filled: true,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: streetAdressController,
                    keyboardType: TextInputType.streetAddress,
                    decoration: const InputDecoration(
                      hintText: 'Endereço',
                      fillColor: Colors.white70,
                      filled: true,
                    ),
                  ),
                ),
                ElevatedButton(
                    onPressed: () async {
                      print(_formKey.currentState!.validate());
                      if (_formKey.currentState!.validate()) {
                        dynamic response = await SignUpService().sign_up(
                            emailController.text,
                            passwordController.text,
                            phoneController.text,
                            streetAdressController.text,
                            name.text,
                            typeAccountController.text,
                            typeDocument.text,
                            description.text
                        );
                        if (response != null) {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()),
                          );
                        }
                      }
                    },
                    child: const Text('Cadastrar')),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Cancelar'),
                    style: const ButtonStyle(
                        backgroundColor:
                            MaterialStatePropertyAll(Colors.redAccent))),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
