import 'package:flutter/material.dart';
import 'package:frontend/controller/services/feeds.dart';
import 'package:frontend/view/components/list_machine_feed.dart';
import 'package:frontend/view/pages/list_machine.dart';
import '../../models/feeds.model.dart';

class ListFeed extends StatefulWidget {
  const ListFeed({super.key});

  @override
  State<ListFeed> createState() => _ListFeedState();
}

class _ListFeedState extends State<ListFeed> {
  TextEditingController _filtroController = TextEditingController();
  List<Lojas> _lojas = [];
  List<Lojas> _lojasOriginais = [];

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  void _loadData() async {
    List<Lojas> lojas = await getFeeds();
    setState(() {
      _lojas = lojas;
      _lojasOriginais = List.from(lojas);
    });
  }

  void _filtrarLojas(String filtro) {
    List<Lojas> lojasFiltradas = _lojasOriginais.where((loja) {
      return loja.name.toLowerCase().contains(filtro.toLowerCase());
    }).toList();

    setState(() {
      _lojas = lojasFiltradas;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.indigo),
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text('Feed Ibricks'),
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _filtroController,
                onChanged: (filtro) {
                  _filtrarLojas(filtro);
                },

                decoration: const InputDecoration(
                  labelText: 'Filtrar Lojas',
                ),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: List.generate(
                    _lojas.length,
                        (index) => InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ListMachine(
                              loja: _lojas[index],
                            ),
                          ),
                        );
                      },
                      child: Card(
                        child: Container(
                          width: 250,
                          height: 300,// Ajuste conforme necessário
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  color: Colors.black26,
                                ),
                                width: 230,
                                height: 150, // Ajuste conforme necessário
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.asset("assets/fachada.jpg", fit: BoxFit.cover),
                                ),
                              ),
                              Container(
                                height: 150,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        children: [
                                          Text(
                                            _lojas[index].name,
                                            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                                          ),
                                          Text('Avaliação: ${_lojas[index].avaliation}'),
                                        ],
                                      ),
                                    ),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Text('Entrega: ${_lojas[index].time}'),
                                        Text('Preço: ${_lojas[index].price}'),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            const Expanded(
                child: ListMachineFeed()
            )
          ],
        ),
      ),
    );
  }
}
