import 'package:flutter/foundation.dart';
import 'package:frontend/models/login_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

ResponseLogin parseResponseLogin(String responseLogin) {
  final Map<String, dynamic> parsed = jsonDecode(responseLogin);

  return ResponseLogin.fromJson(parsed);
}

class LoginService {
  Future<ResponseLogin> login(String email, String senha) async {
    final response = await http.post(
      Uri.parse('http://192.168.1.13:4000/api/login'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{'email': email, 'senha': senha}),
    );
    if (response.statusCode == 200) {
      print(compute(parseResponseLogin, response.body));
      return compute(parseResponseLogin, response.body);
    } else {
      throw Exception('Error');
    }
  }
}
