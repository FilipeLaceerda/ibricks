import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:frontend/models/machines_model.dart';
import 'package:http/http.dart' as http;

List<Tools> parseTools(String responsebody) {
  final parsed = jsonDecode(responsebody).cast<Map<String, dynamic>>();

  return parsed.map<Tools>((json) => Tools.fromJson(json)).toList();
}

Future<List<Tools>> getListMachines(int storesId) async {
  final response = await http
      .get(Uri.parse('http://192.168.1.13:3003/stores/$storesId/machines'));

  if(response.statusCode == 200){
    return compute(parseTools, response.body);
  } else {
    throw Exception('Falha em carregar a lista de maquinas');
  }
}