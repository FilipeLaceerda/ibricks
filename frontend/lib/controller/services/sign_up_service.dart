import 'package:http/http.dart' as http;
import 'dart:convert';

class SignUpService {
  Future<dynamic> sign_up(
      String email,
      String senha,
      String phone,
      String streetAdress,
      String nome,
      String tipo,
      String documento,
      String descricao) async {
    final response = await http.post(
      Uri.parse('http://192.168.1.13:4000/api/sign-up'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'email': email,
        'senha': senha,
        'endereco': streetAdress,
        'numeroTelefone': phone,
        'nome': nome,
        'tipo': tipo,
        'documento': documento,
        'descricao': descricao
      }),
    );
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      print(response);
      throw Exception('Error');
    }
  }
}
