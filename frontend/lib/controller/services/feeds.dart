import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../../models/feeds.model.dart';

List<Lojas> parseLojas(String responsebody) {
  final parsed = jsonDecode(responsebody).cast<Map<String, dynamic>>();

  return parsed.map<Lojas>((json) => Lojas.fromJson(json)).toList();
}

Future<List<Lojas>> getFeeds() async {
  final response = await http
      .get(Uri.parse('http://192.168.1.13:3003/stores'));

  if(response.statusCode == 200){
    return compute(parseLojas, response.body);
  } else {
    print(response.statusCode);
    throw Exception('Falha em carregar os feeds');
  }
}

List<Machines> parseMachines(String responsebody) {
  final parsed = jsonDecode(responsebody).cast<Map<String, dynamic>>();

  return parsed.map<Machines>((json) => Machines.fromJson(json)).toList();
}

Future<List<Machines>> getMachines() async {
  final response = await http
      .get(Uri.parse('http://192.168.1.13:3003/machines'));

  if(response.statusCode == 200){
    print(response.body);
    return compute(parseMachines, response.body);
  } else {
    print(response.statusCode);
    throw Exception('Falha em carregar os máquinas e ferramentas');
  }
}