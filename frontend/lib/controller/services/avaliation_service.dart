import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:frontend/models/coments_model.dart';
import 'package:http/http.dart' as http;
import 'package:frontend/models/rating_model.dart';

Rating parseRating(String responsebody) {
  final Map<String, dynamic> parsed = jsonDecode(responsebody);

  return Rating.fromJson(parsed);
}

Future<Rating> getRating(int storesReviewId) async {
  final response = await http
      .get(Uri.parse('http://192.168.1.13:3002/api/avaliations/$storesReviewId/rating'));

  if(response.statusCode == 200){
    return compute(parseRating, response.body);
  } else {
    throw Exception('Falha em carregar a classificação da loja');
  }
}

CountComent parseCountComent(String responsebody) {
  final Map<String, dynamic> parsed = jsonDecode(responsebody);

  return CountComent.fromJson(parsed);
}

Future<CountComent> getCountComent(int storesReviewId) async {
  final response = await http.get(
      Uri.parse('http://192.168.1.13:3002/api/avaliations/$storesReviewId/count-coments'));

  if (response.statusCode == 200) {
    return compute(parseCountComent, response.body);
  } else {
    throw Exception('Falha em carregar a quantidade de comentários da loja');
  }
}

List<Coments> parseComments(String responsebody) {
  final parsed = jsonDecode(responsebody).cast<Map<String, dynamic>>();

  return parsed.map<Coments>((json) => Coments.fromJson(json)).toList();
}

Future<List<Coments>> getListComments(int storesReviewId) async {
  final response = await http
      .get(Uri.parse('http://192.168.1.13:3002/api/avaliations/$storesReviewId/coments'));

  if(response.statusCode == 200){
  print(response.body);
    return compute(parseComments, response.body);
  } else {
    throw Exception('Falha em carregar a lista de maquinas');
  }

}
  Future<dynamic> registerComent(String coment, int storesReviewId) async {
    final response = await http.post(
        Uri.parse('http://192.168.1.13:3002/api/avaliations/$storesReviewId/register-coments'),
        headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{'coment': coment})
    );
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Error');
    }
  }
