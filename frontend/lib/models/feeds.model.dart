import 'dart:ffi';

class Lojas {
  final int id;
  final String name;
  final String time;
  final String avaliation;
  final String price;

  const Lojas(
      {required this.id,
      required this.name,
      required this.time,
      required this.avaliation,
      required this.price});

  factory Lojas.fromJson(Map<String, dynamic> json) {
    return Lojas(
        id: json['id'] as int,
        name: json['name'] as String,
        time: json['time'] as String,
        avaliation: json['avaliation'] as String,
        price: json['price'] as String);
  }
}

class Machines {
  final int id;
  final String nome;
  final String tipo;
  final bool disponivel;
  final double priceProduct;
  final int storesId;

  const Machines({
    required this.id,
    required this.nome,
    required this.tipo,
    required this.disponivel,
    required this.priceProduct,
    required this.storesId,
  });

  factory Machines.fromJson(Map<String, dynamic> json) {
    return Machines(
        id: json['id'] as int,
        nome: json['nome'] as String,
        tipo: json['tipo'] as String,
        disponivel: json['disponivel'] as bool,
        priceProduct: json['price_product'] == null ? 0.0 : json['price_product'].toDouble(),
        storesId: json['stores_id'] as int);
  }
}
