class ResponseLogin {
  Permissao permissao;
  String token;

  ResponseLogin({
    required this.permissao,
    required this.token,
  });

  factory ResponseLogin.fromJson(Map<String, dynamic> json) {
    return ResponseLogin(
        permissao: json['permissao'] as Permissao,
        token: json['token'] as String,
    );
  }
}

class Permissao {
  int id;
  int usuarioId;
  String tipo;

  Permissao({
    required this.id,
    required this.usuarioId,
    required this.tipo,
  });
}