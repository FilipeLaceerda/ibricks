class Coments {
  final int id;
  final String coment;
  final int store_review_id;

  const Coments({
    required this.id,
    required this.coment,
    required this.store_review_id,
  });

  factory Coments.fromJson(Map<String, dynamic> json) {
    return Coments(
        id: json['id'] as int,
        coment: json['coment'] as String,
        store_review_id: json['store_review_id'] as int
    );
  }
}

class CountComent {
  final int count;

  const CountComent({
    required this.count
  });

  factory CountComent.fromJson(Map<String, dynamic> json) {
    return CountComent(count: json['count'] as int);
  }
}