class Tools {
  final int id;
  final String nome;
  final String tipo;
  final double price_product;
  final bool disponivel;
  final int stores_id;

  const Tools({
    required this.id,
    required this.nome,
    required this.tipo,
    required this.price_product,
    required this.disponivel,
    required this.stores_id
  });

  factory Tools.fromJson(Map<String, dynamic> json) {
    return Tools(
        id: json['id'] as int,
        nome: json['nome'] as String,
        tipo: json['tipo'] as String,
        price_product: json['price_product'] as double,
        disponivel: json['disponivel'] as bool,
        stores_id: json['stores_id'] as int
    );
  }
}