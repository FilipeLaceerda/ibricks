class Rating {
  final double rating;

  const Rating({
    required this.rating,
  });

  factory Rating.fromJson(Map<String, dynamic> json) {
    return Rating(
        rating: json['rating'] as double,
    );
  }
}